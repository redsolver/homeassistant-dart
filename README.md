## HomeAssistant-Dart
![HomeAssistant Icon](https://upload.wikimedia.org/wikipedia/commons/thumb/6/6e/Home_Assistant_Logo.svg/240px-Home_Assistant_Logo.svg.png)
HomeAssistant-Dart is a dart bindings package modeled after the official python bindings. 

**Currently implemented methods:**

 - validate_api
 - set_state
 - get_state
 - get_states
 - is_state
 - get_services
 - call_services
 - get_config

## Usage
A simple usage example:

    import 'package:homeassistant_dart/homeassistant_dart.dart' as remote;
    main() async{
      var api = remote.API('192.168.1.100', 'password', 'port');
      print(await remote.validate_api(api));
    }

  

Created from templates made available by Stagehand under a BSD-style

[license](https://github.com/dart-lang/stagehand/blob/master/LICENSE).
