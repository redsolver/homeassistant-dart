// TODO: Put public facing types in this file.

/// Checks if you are awesome. Spoiler: you are.
/// 
import 'homeassistant_dart_const.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:uri/uri.dart';
import 'dart:async';
import 'package:sprintf/sprintf.dart';

class Awesome {
  bool get isAwesome => true;
}

class APIStatus {
  final OK = 'ok';
  final INVALID_PASSWORD = 'invalid_password';
  final CANNOT_CONNECT = 'cannot_connect';
  final UNKNOWN = 'unknown';
}

class API {
  String base_url;
  APIStatus status;
  Map<String, String> headers;
  var base_url_template;

  API(String host, [String api_password = null, String port = SERVER_PORT, bool use_ssl = false]){
    if(host.startsWith('http://') || host.startsWith('https://')){
      base_url = host;
    }
    else if (use_ssl) {
      base_url = 'https://$host';
    }
    else{
      base_url = 'http://$host';
    }

    if (port != null){
      base_url += ':$port';
    }

    base_url_template = new UriTemplate('$base_url{path}');

    status = null;
    headers = {CONTENT_TYPE: CONTENT_TYPE_JSON};

    if (api_password != null){
      headers[HTTP_HEADER_HA_AUTH] = api_password;
    }
  }

  call(String method, String path, [Map data = null]) async{
      String request_data = null;
      if (data != null){
        request_data = json.encode(data);
      }
      String url = Uri.encodeFull(base_url + path);
      var response = null;
      try{
        switch (method) {
          case METH_GET:
            response = await http.get(url, headers: headers);
            break;
          case METH_POST:
            response = await http.post(url, headers: headers, body: request_data);
            break;
          case METH_DELETE:
            response = await http.delete(url, headers: headers);
        }
      } catch(e){
        print('Error making call to server');
        print(e);
      }
      
      return response;
    }
}

Future<String> validate_api(api) async{
  APIStatus status = new APIStatus();
  try{
    var req = await api(METH_GET, URL_API);
    if(req.statusCode == 200){
      return status.OK;
    }
    else if(req.statusCode == 401){
      return status.INVALID_PASSWORD;
    }
    else {
      return status.UNKNOWN;
    }
  } catch(e){
    print('Cannot connect to server.');
    print(e);
    return status.CANNOT_CONNECT;
  }
  
}

Future<bool> set_state(api, entity_id, new_state, [attributes=null, force_update=false]) async{
  if(attributes == null){
    attributes = {};
  }

  Map data = {
    'state': new_state,
    'attributes': attributes,
    'force_update': force_update
  };

  try{
    var req = api(METH_POST, sprintf(URL_API_STATES_ENTITY, [entity_id]), data);
    if(req.statusCode == 200 || req.statusCode == 201){
      return true;
    }
    else{
      throw 'Server returned error ${req.statusCode}';
    }
  } catch(e){
    print(e);
  }
}

Future<Map> get_state(api, String entity_id) async{
  try{
    var req = await api(METH_GET, sprintf(URL_API_STATES_ENTITY, [entity_id]));
    if(req.statusCode == 200){
      return json.decode(req.body);
    }
    else{
      throw 'Server returned error ${req.statusCode}';
    }
  } on NoSuchMethodError{
    print('Request failed!');
  } catch (e) {
    print('Error fething states');
    print(e);
  }
} 

Future<List> get_states(api) async{
  try{
    var req = await api(METH_GET, URL_API_STATES);
    if(req.statusCode == 200){
      return json.decode(req.body);
    }
    else{
      throw 'Server returned error ${req.statusCode}';
    }
  } catch(e){
    print(e);
  }
}

Future<bool> is_state(api, entity_id, state) async{
  var current_state = await get_state(api, entity_id);
  if(state == current_state){
    return true;
  }
  return false;
}

Future<List> get_services(api) async{
  try{
    var req = await api(METH_GET, URL_API_SERVICES);
    if(req.statusCode == 200){
      return json.decode(req.body);
    }
    else {
      throw 'Server returned error ${req.statusCode}';
    }
  } catch(e){
    print(e);
  }
}

Future<String> call_service(api, domain, service, [service_data = null]) async{
  try{
    var req = await api(
                METH_POST, 
                sprintf(URL_API_SERVICES_SERVICE, [domain, service]),
                service_data);
    if(req.statusCode == 200){
      return 'Service called.';
    }
    else{
      throw 'Server returned error ${req.statusCode}';
    }
  } catch(e){
    print(e);
  }
}

Future<Map> get_config(api) async{
  try{
    var req = await api(METH_GET, URL_API_CONFIG);
    if(req.statusCode == 200){
      return json.decode(req.body);
    }
    else {
      throw 'Server returned error ${req.statusCode}';
    }
  } catch(e){
    print(e);
  }
}

