//Home Assistant Constants
const URL_API = '/api/';
const SERVER_PORT = '8123';
const URL_API_CONFIG = '/api/config';
const URL_API_EVENTS = '/api/events';
const URL_API_STATES = '/api/states';
const URL_API_SERVICES = '/api/services';
const CONTENT_TYPE_JSON = 'application/json';
const HTTP_HEADER_HA_AUTH = 'X-HA-access';
const URL_API_EVENTS_EVENT = '/api/events/%s';
const URL_API_STATES_ENTITY = '/api/states/%s';
const URL_API_SERVICES_SERVICE = '/api/services/%s/%s';

//Requests constants
const METH_GET = 'GET';
const METH_POST = 'POST';
const METH_DELETE = 'DELETE';
const CONTENT_TYPE = 'Content-Type';